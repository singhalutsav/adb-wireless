package in.blogspot.choosejava.adbwireless;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView instructionsTextView;
    private TextView commandDetailsTextView;

    private Button wifiIPDisplayButton;
    private Button settingsButton;
    private Button viewOnPlayStoreButton;
    private Button shareAppButton;

    private String commandText;
    private String commandDetailsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initViews();
        setOnClickListeners();
        String instructionsText = Utils.readRawTextFile(this, R.raw.app_text);
        instructionsTextView.setText(instructionsText);
    }

    @Override
    protected void onResume() {
        super.onResume();
        wifiIPDisplayButton.performClick();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        commandDetailsTextView = findViewById(R.id.main_command_details_textView);
        instructionsTextView = findViewById(R.id.main_instructions_textView);

        wifiIPDisplayButton = findViewById(R.id.main_wifi_ip_button);
        settingsButton = findViewById(R.id.main_settings_button);
        viewOnPlayStoreButton = findViewById(R.id.main_play_store_button);
        shareAppButton = findViewById(R.id.main_share_button);
    }

    private void setOnClickListeners(){
        wifiIPDisplayButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
        viewOnPlayStoreButton.setOnClickListener(this);
        shareAppButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.main_wifi_ip_button:{
                commandDetailsTextView.setText("");
                commandText = Utils.getCommandText(this);

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                boolean isNotificationPreferenceSet = sharedPref.getBoolean("notifications_new_message", true);

                if(!commandText.equals(getString(R.string.wifi_not_connected))){
                    //WIFI is available
                    StringBuilder commandTextTemp = new StringBuilder("Run the following command:\n");
                    commandTextTemp.append(commandText);
                    commandDetailsText = commandTextTemp.toString();

                    commandDetailsTextView.setText(commandDetailsText);

                    if(isNotificationPreferenceSet){
                        Utils.notify(this, commandText);
                    }

                    Toast.makeText(this, commandDetailsText, Toast.LENGTH_SHORT).show();
                } else{
                    //WIFI unavailable
                    //Just show message
                    commandDetailsTextView.setText(commandText);
                    Toast.makeText(this, commandText, Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case R.id.main_settings_button:{
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.main_play_store_button:{
                Utils.openPlayStoreListing(this);
                break;
            }

            case R.id.main_share_button:{
                Utils.shareApp(this);
                break;
            }
        }
    }
}
