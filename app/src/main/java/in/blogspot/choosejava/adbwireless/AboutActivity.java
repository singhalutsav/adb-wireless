package in.blogspot.choosejava.adbwireless;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AboutActivity extends AppCompatActivity {

    private TextView aboutTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aboutTextView = findViewById(R.id.about_textView);
        String aboutText = Utils.readRawTextFile(this, R.raw.about_text);
        aboutTextView.setText(aboutText);
    }
}
