package in.blogspot.choosejava.adbwireless;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.format.Formatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import androidx.core.app.NotificationCompat;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by utsavsinghal on 04/04/18.
 */

public class Utils {
    public static String getCommandText(Context context){
        if(!isWifiConnected(context)){
          return context.getString(R.string.wifi_not_connected);
        }

        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        String ipAddress = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        StringBuilder commandText = new StringBuilder();
        commandText.append("adb connect ");
        commandText.append(ipAddress);
        commandText.append(":5555");

        return commandText.toString();
    }

    private static boolean isWifiConnected(Context context){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }

    public static String readRawTextFile(Context ctx, int resId){
        InputStream inputStream = ctx.getResources().openRawResource(resId);
        InputStreamReader inputReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputReader);

        String line = null;
        StringBuilder text = new StringBuilder();

        try {
            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
                text.append("\n");
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }

    public static void notify(Context context, String commandText){
        initChannels(context);
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = null;
        //PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S){
            pendingIntent = PendingIntent.getActivity
                    (context, 0, intent, PendingIntent.FLAG_MUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity
                    (context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        }

        NotificationCompat.Builder b = new NotificationCompat.Builder(context, "default");

        b.setAutoCancel(true)
                .setSound(null)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher_main)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(commandText)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, b.build());
    }

    public static void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("default",
                "My channel",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }

    public static void shareApp(Context context){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
            StringBuilder shareAppTemp = new StringBuilder();
            shareAppTemp.append("\nLet me recommend you this application\n\n");
            shareAppTemp.append(context.getString(R.string.share_app_link));
            String shareApp = shareAppTemp.toString();
            i.putExtra(Intent.EXTRA_TEXT, shareApp);
            context.startActivity(Intent.createChooser(i, "Choose one"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void openPlayStoreListing(Context context){
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
